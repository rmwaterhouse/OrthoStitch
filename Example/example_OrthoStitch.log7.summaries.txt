Running OrthoStitch v1.6 - March 2017
OPTIONS: OrthoStitch.pl -g example_anchor_groups.txt -l example_anchor_locations.txt -d middle -u no -o example
STARTED: Mon Aug 27 12:15:49 CEST 2018

# 1. Anchor group data summary
Total number of lines: 115150
Total number of unique genes: 115150
Total number of unique groups: 16929
Total number of unique species: 10
	AALBI	10637 genes
	AATRO	12249 genes
	ACOLU	12998 genes
	ADARL	9871 genes
	ADIRU	11488 genes
	AEPIR	11549 genes
	AGAMB	12240 genes
	AMINI	11436 genes
	ASINS	11037 genes
	ASTES	11645 genes

# 2. Anchor location data summary
Total number of lines: 526016
Total number of unique genes: 127664
Total number of unique species: 10
	AALBI	52347 CDSs
	AATRO	57335 CDSs
	ACOLU	56639 CDSs
	ADARL	47990 CDSs
	ADIRU	53615 CDSs
	AEPIR	50396 CDSs
	AGAMB	53541 CDSs
	AMINI	53698 CDSs
	ASINS	46703 CDSs
	ASTES	53752 CDSs

# 3. Anchor usage
useall: no
defloc: middle
	AALBI	10637 genes	52 chromosomes/scaffolds
	AATRO	12249 genes	384 chromosomes/scaffolds
	ACOLU	12998 genes	816 chromosomes/scaffolds
	ADARL	9871 genes	2055 chromosomes/scaffolds
	ADIRU	11488 genes	250 chromosomes/scaffolds
	AEPIR	11549 genes	1004 chromosomes/scaffolds
	AGAMB	12240 genes	7 chromosomes/scaffolds
	AMINI	11436 genes	121 chromosomes/scaffolds
	ASINS	11037 genes	1486 chromosomes/scaffolds
	ASTES	11645 genes	479 chromosomes/scaffolds

# 4. Neighbour landscape
Species ID, 2 neighbours, 1 neighbour, 0 neighbours
	AALBI	10541	88	8
	AATRO	11701	328	220
	ACOLU	11935	494	569
	ADARL	6586	2460	825
	ADIRU	11117	242	129
	AEPIR	9799	1492	258
	AGAMB	12226	14	0
	AMINI	11252	126	58
	ASINS	8898	1306	833
	ASTES	10764	804	77

# 5. Possible adjacencies
Species ID, pairs, complex, resolved, all, confident, superconfident
	AALBI	5	0	0	5	5	3
	AATRO	34	10	2	36	32	7
	ACOLU	126	45	18	144	133	17
	ADARL	688	174	79	767	715	160
	ADIRU	47	12	6	53	48	18
	AEPIR	489	47	16	505	456	278
	AGAMB	0	0	0	0	0	0
	AMINI	16	2	0	16	13	9
	ASINS	254	93	21	275	259	22
	ASTES	226	20	7	233	209	131

# 6. Final adjacencies
Species ID, all, confident, superconfident
	AALBI	5	5	3
	AATRO	36	32	7
	ACOLU	138	127	17
	ADARL	730	681	159
	ADIRU	53	48	18
	AEPIR	505	456	278
	AGAMB	0	0	0
	AMINI	16	13	9
	ASINS	265	249	22
	ASTES	233	209	131


COMPLETED: Mon Aug 27 12:15:55 CEST 2018

