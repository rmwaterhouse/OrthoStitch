# Author: Robert Waterhouse
# Copyright Robert M. Waterhouse 2017
# OrthoStitch v1.6 - March 2017
#!/usr/bin/perl

##### INPUT requirements #####

#@# 1. Anchor Groups File
# Three tab-separated columns (NO HEADER):
# 1st column = group ID, e.g. orthologous group ID
# 2nd column = gene ID, e.g. AALB000006 (must correspond exactly to gene IDs used in anchor locations file)
# 3rd column = species ID, e.g. AALBS (must correspond exactly to species IDs used in anchor locations file)
# NOTE: a gene cannot belong to more than one group
# NOTE: gene IDs should be unique across the complete dataset, e.g. cannot have GENE0001 from both species A and species B
# NOTE: group IDs and gene IDs should not contain any colons ':' or semi-colons ';'

#@# 2. Anchor Locations File
# Nine tab-seperated columns [like GFF] (NO HEADER, ALL SPECIES IN A SINGLE FILE):
# 1st column = chromosome or scaffold ID
# 2nd column = species ID (must correspond exactly to species ID used in anchor groups file, normally the 'source' column in GFF)
# 3rd column = CDS
# 4th column = start
# 5th column = end
# 6th column = score (not used, kept only to match GFF)
# 7th column = strand ('+' or '-')
# 8th column = phase (not used, kept only to match GFF)
# 9th column = gene ID (must correspond exactly to gene IDs used in anchor groups file)
# Note: can easily be created from most GFF files
# - select only CDS lines
# - replace 'source' with the species ID that corresponds to the species IDs used in the anchor groups file
# - make sure 9th column ('attribute' in normal GFF files) contains only the gene ID, e.g. AALB000006 not gene_id="AALB000006";

# NOTE: the anchor locations file may contain the locations of genes that are not present in the anchor groups file, i.e.
# some genes with known locations may not have been assigned to any group, however, the anchor groups file may not contain
# any genes that are not present in the anchor locations files, i.e. all genes in groups must have known locations

#####@@@@@#####


##### USAGE #####

# Five variables MUST be passed, in the following order, to control input, analysis options, and output:

#@# 1. -groups /path/to/anchor/groups/file
# -g = -groups
# the anchor groups file

#@# 2. -locations /path/to/anchor/locations/file
# -l = -locations
# the anchor locations file

#@# 3. -defloc start OR end OR middle
# -d = -defloc
# select how to define gene location: use start, end or middle (middle = midway between start and end)
# this is strand-aware, i.e. start is N-terminus and end is C-terminus
# if genes have the same start/end/middle position they are sorted alphanumerically

#@# 4. -useall yes OR no
# -u = -useall
# select whether to use all genes (yes), or only genes present in anchor groups (no)

#@# 5. -out name
# -o = -out
# the prefix for all output files from the analysis

#@# EXAMPLES:
# perl OrthoStitch.pl -groups /home/user1/directory1/my_anchor_groups_file.txt -locations /home/user1/directory1/my_anchor_locations_file.txt -defloc middle -useall yes -out run1
# perl OrthoStitch.pl -groups /home/user1/directory1/my_anchor_groups_file.txt -locations /home/user1/directory1/my_anchor_locations_file.txt -defloc start -useall no -out run2
# perl OrthoStitch.pl -g /home/user1/directory1/my_anchor_groups_file.txt -l /home/user1/directory1/my_anchor_locations_file.txt -d middle -u no -o run3
# perl OrthoStitch.pl -g /home/user1/directory1/my_anchor_groups_file.txt -l /home/user1/directory1/my_anchor_locations_file.txt -defloc end -u no -o run4
# perl OrthoStitch.pl -groups /home/user1/directory1/my_anchor_groups_file.txt -l /home/user1/directory1/my_anchor_locations_file.txt -d middle -u yes -out run5

#####@@@@@#####

use strict;
my $osversion='v1.6 - March 2017';

# check argument tags and variables
if(scalar(@ARGV) != 10) {
    print "Error: there should be 5 pairs of argument tags and variables.\n";
    print "E.g. perl OrthoStitch.pl -groups my_anchor_groups_file.txt -locations my_anchor_locations_file.txt -defloc middle -useall yes -out run1\n\n";
    exit(0);
} 
if($ARGV[0] ne '-groups') {
    if($ARGV[0] ne '-g') {
	print "Error: first argument tag should be '-groups' or '-g', you entered '$ARGV[0]'\n\n";
	exit(0);
    }    
}
my $groups=$ARGV[1];
if($ARGV[2] ne '-locations') {
    if($ARGV[2] ne '-l') {
	print "Error: second argument tag should be '-locations' or '-l', you entered '$ARGV[2]'\n\n";
	exit(0);
    }    
}
my $locations=$ARGV[3];
if($ARGV[4] ne '-defloc') {
    if($ARGV[4] ne '-d') {
	print "Error: third argument tag should be '-defloc' or '-d', you entered '$ARGV[4]'\n\n";
	exit(0);
    }    
}
if($ARGV[5] ne 'start') {
    if($ARGV[5] ne 'end') {
	if($ARGV[5] ne 'middle') {
	    print "Error: third argument (defloc) value should be 'start' or 'end' or 'middle', you entered '$ARGV[5]'\n\n";
	    exit(0);
	}   
    } 
}
my $defloc=$ARGV[5];
if($ARGV[6] ne '-useall') {
    if($ARGV[6] ne '-u') {
	print "Error: fourth argument tag should be '-useall' or '-u', you entered '$ARGV[6]'\n\n";
	exit(0);
    }    
}
if($ARGV[7] ne 'yes') {
    if($ARGV[7] ne 'no') {
	print "Error: fourth argument (useall) should be 'yes' or 'no', you entered '$ARGV[7]'\n\n";
	exit(0);
    }    
}
my $useall=$ARGV[7];
if($ARGV[8] ne '-out') {
    if($ARGV[8] ne '-o') {
	print "Error: fifth argument tag should be '-out' or '-o', you entered '$ARGV[8]'\n\n";
	exit(0);
    }    
}
my $run=$ARGV[9];

# read in group information
my @grouplines;
if(-f $groups) {
    open(ING,$groups) || die $!;
    @grouplines=<ING>;
    close(ING);
    print "\nReading anchor groups file: $groups ... ";
    foreach my $line (@grouplines) {
	my @bits=split(/\t/,$line);
	if(scalar(@bits)!=3) { 
	    print "\nError: anchor groups file should contain 3 columns of data - see\n$line\n";
	    exit(0);
	}
    }
    print "OK.\n";
}
else {
    print "Cannot find anchor groups file '$groups'\n\n";
    exit(0);
}

# read in location information
my @locationlines;
if(-f $locations) {
    open(INL,$locations) || die $!;
    @locationlines=<INL>;
    close(INL);
    print "Reading anchor locations file: $locations ... ";
    foreach my $line (@locationlines) {
	my @bits=split(/\t/,$line);
	if(scalar(@bits)!=9) { 
	    print "\nError: anchor locations file should contain 9 columns of data - see\n$line\n";
	    exit(0);
	}
    }
    print "OK.\n";
}
else {
    print "Cannot find anchor locations file '$locations'\n\n";
    exit(0);
}

# if run not already present then open required handles
if(-f "$run\_OrthoStitch.log1.genes.txt") {
    print "Error: output files with the same name already exist, delete or choose a new name for this run.\n\n";
    exit(0);
}
else {
    open(LOG,">$run\_OrthoStitch.log1.genes.txt") || die $!;
    open(INP,">$run\_OrthoStitch.log7.summaries.txt") || die $!;
    print INP "Running OrthoStitch $osversion\n";
    print INP "OPTIONS: OrthoStitch.pl " . join(" ", @ARGV) . "\n";
    print INP "STARTED: " . `date` . "\n";
}

# gather anchor group data
undef my %og2gns;
undef my %gn2og;
undef my %gn2sp;
undef my %ancsp;
foreach my $line (@grouplines) {
    chomp($line);
    my ($og,$gn,$sp)=split(/\t/,$line);
    push(@{$og2gns{$og}},$gn);
    $gn2og{$gn}=$og;
    $gn2sp{$gn}=$sp;
    if(defined($ancsp{$sp})) { $ancsp{$sp}++; }
    else { $ancsp{$sp}=1; }
}
print INP "# 1. Anchor group data summary\n";
print INP "Total number of lines: " . scalar(@grouplines) . "\n";
print INP "Total number of unique genes: " . scalar(keys %gn2og) . "\n";
print INP "Total number of unique groups: " . scalar(keys %og2gns) . "\n";
print INP "Total number of unique species: " . scalar(keys %ancsp) . "\n";
foreach my $sp (sort keys %ancsp) {
    print INP "\t$sp\t$ancsp{$sp} genes\n";
}
print INP "\n";

# remove groups with too many paralogs
# where #genes / #species > 10
undef my %og2gns2;
undef my %gn2og2;
my @removedogs=();
foreach my $og (sort keys %og2gns) {
    my %ogsps;
    foreach my $gn (@{$og2gns{$og}}) { $ogsps{$gn2sp{$gn}}=1; }
    if( (scalar(@{$og2gns{$og}}) / scalar(keys %ogsps)) <= 10 ) { 
	push(@{$og2gns2{$og}},@{$og2gns{$og}}); 
	foreach my $gn (@{$og2gns{$og}}) { $gn2og2{$gn}=$gn2og{$gn}; }
    }
    else { push(@removedogs,$og); }
}
undef %og2gns;
%og2gns=%og2gns2;
undef %gn2og;
%gn2og=%gn2og2;
if(scalar(@removedogs)>0) {
    print INP "NOTE: ignoring " . scalar(@removedogs) . " groups where genes:species ratio > 10\n\n";
}

# gather anchor location data
undef my %gn2all;
undef my %gn2chr;
undef my %gn2dir;
undef my %locsp;
foreach my $line (@locationlines) {
    chomp($line);
    my @bits=split(/\t/,$line);
    push(@{$gn2all{$bits[8]}},$bits[3],$bits[4]);
    $gn2chr{$bits[8]}=$bits[0];
    $gn2dir{$bits[8]}=$bits[6];
    if(defined($locsp{$bits[1]})) { $locsp{$bits[1]}++; }
    else { $locsp{$bits[1]}=1; }
    if(!defined($ancsp{$bits[1]})) {
	print "Error: anchor locations file contains species ($bits[1]) not found in anchor groups file.\n$line\n\n";
	exit(0);
    }
    if(defined($gn2sp{$bits[8]})) {
	if($gn2sp{$bits[8]} ne $bits[1]) {
	    print "Error: species in locations file ($bits[1]) not the same as species in groups file ($gn2sp{$bits[8]}) for gene: $bits[8]\n\n";
	    exit(0);
	}
    }
    else { $gn2sp{$bits[8]}=$bits[1]; }
}
print INP "# 2. Anchor location data summary\n";
print INP "Total number of lines: " . scalar(@locationlines) . "\n";
print INP "Total number of unique genes: " . scalar(keys %gn2chr) . "\n";
print INP "Total number of unique species: " . scalar(keys %locsp) . "\n";
foreach my $sp (sort keys %locsp) {
    print INP "\t$sp\t$locsp{$sp} CDSs\n";
}
print INP "\n";

# set genomic positions per gene
print "Setting gene locations by: $defloc ... ";
undef my %gn2pos;
undef my %sp2gns;
undef my %sp2chr;
undef my %chr2gns;
foreach my $gn (keys %gn2all) {
    my @bits = sort { $a <=> $b } @{$gn2all{$gn}};
    if($defloc eq 'middle') { $gn2pos{$gn} = sprintf("%.0f", ( (($bits[$#bits]-$bits[0])/2)+$bits[0] ) ); }
    elsif($defloc eq 'start') { 
	if($gn2dir{$gn} eq '+') { $gn2pos{$gn} = $bits[0]; }
	else { $gn2pos{$gn} = $bits[$#bits]; }
    }
    elsif($defloc eq 'end') { 
	if($gn2dir{$gn} eq '+') { $gn2pos{$gn} = $bits[$#bits]; }
	else { $gn2pos{$gn} = $bits[0]; }
    }
    my $sp=$gn2sp{$gn};
    if($useall eq 'yes') {
	push(@{$sp2gns{$sp}},$gn);
	push(@{$chr2gns{"$sp:$gn2chr{$gn}"}},$gn); 
	push(@{$sp2chr{$sp}},$gn2chr{$gn});
    }
    elsif($useall eq 'no') {
	if(defined($gn2og{$gn})) {
	    push(@{$sp2gns{$sp}},$gn);
	    push(@{$chr2gns{"$sp:$gn2chr{$gn}"}},$gn); 
	    push(@{$sp2chr{$sp}},$gn2chr{$gn});
	}
    }
}
print INP "# 3. Anchor usage\n";
print INP "useall: $useall\n";
print INP "defloc: $defloc\n";
foreach my $sp (sort keys %sp2gns) {
    undef my %seen;
    foreach my $chr (@{$sp2chr{$sp}}) { $seen{$chr}=1; }
    print INP "\t$sp\t" . scalar(@{$sp2gns{$sp}}) . " genes\t" . scalar(keys %seen) . " chromosomes/scaffolds\n";
}
print INP "\n";
print "OK.\n";

# check all anchors in groups have locations
my @nolocs=();
foreach my $gn (keys %gn2og) {
    if(!defined($gn2pos{$gn})) { push(@nolocs,$gn); }
}
if(scalar(@nolocs)>0) {
    print "Error: " . scalar(@nolocs) . " anchor(s) from the groups file has/have no location data\n";
    print join(" ",sort(@nolocs)) . "\n";
    exit(0);
}

# define gene neighbours
print "Defining gene neighbours ................ ";
undef my %gene2nbrs;
undef my %lonely;
undef my %sp2nbc;
foreach my $chr (sort keys %chr2gns) {
    my @bits=split(/:/,$chr);
    my $sp=$bits[0];
    undef my %tsort;
    foreach my $gn (@{$chr2gns{$chr}}) {
	$tsort{$gn}=$gn2pos{$gn};
    }
    my @tarr = (sort { $tsort{$a}<=>$tsort{$b} || $a cmp $b } keys %tsort);
    @{$chr2gns{$chr}}=@tarr;

    if(scalar(@{$chr2gns{$chr}})==1) { 
	push(@{$gene2nbrs{${$chr2gns{$chr}}[0]}},'none');
	$lonely{${$chr2gns{$chr}}[0]}=$chr;
	if(defined($sp2nbc{$sp}{'0'})) { $sp2nbc{$sp}{'0'}++; }
	else { $sp2nbc{$sp}{'0'}=1; }
    }
    elsif(scalar(@{$chr2gns{$chr}})==2) { 
	push(@{$gene2nbrs{${$chr2gns{$chr}}[0]}}, ${$chr2gns{$chr}}[1]); 
	push(@{$gene2nbrs{${$chr2gns{$chr}}[1]}}, ${$chr2gns{$chr}}[0]); 
	$lonely{${$chr2gns{$chr}}[0]}=$chr;
	$lonely{${$chr2gns{$chr}}[1]}=$chr;
	if(defined($sp2nbc{$sp}{'1'})) { $sp2nbc{$sp}{'1'}++; $sp2nbc{$sp}{'1'}++; }
	else { $sp2nbc{$sp}{'1'}=2; }
    }
    elsif(scalar(@{$chr2gns{$chr}})>2) { 
	my $term=scalar(@{$chr2gns{$chr}})-1;
	my $penu=$term-1;
	push(@{$gene2nbrs{${$chr2gns{$chr}}[0]}}, ${$chr2gns{$chr}}[1]); 
	push(@{$gene2nbrs{${$chr2gns{$chr}}[$term]}}, ${$chr2gns{$chr}}[$penu]);
	$lonely{${$chr2gns{$chr}}[0]}=$chr;
	$lonely{${$chr2gns{$chr}}[$term]}=$chr;
	if(defined($sp2nbc{$sp}{'1'})) { $sp2nbc{$sp}{'1'}++; $sp2nbc{$sp}{'1'}++; }
	else { $sp2nbc{$sp}{'1'}=2; }
	for(my $i=1; $i<$term; $i++) {
	    my $h=$i-1;
	    my $j=$i+1;
	    push(@{$gene2nbrs{${$chr2gns{$chr}}[$i]}}, ${$chr2gns{$chr}}[$h], ${$chr2gns{$chr}}[$j]);
	    if(defined($sp2nbc{$sp}{'2'})) { $sp2nbc{$sp}{'2'}++; }
	    else { $sp2nbc{$sp}{'2'}=1; }
	}
    }
}
print INP "# 4. Neighbour landscape\n";
print INP "Species ID, 2 neighbours, 1 neighbour, 0 neighbours\n";
foreach my $sp (sort keys %ancsp) {
    if(!defined($sp2nbc{$sp}{'2'})) { $sp2nbc{$sp}{'2'}=0; }
    if(!defined($sp2nbc{$sp}{'1'})) { $sp2nbc{$sp}{'1'}=0; }
    if(!defined($sp2nbc{$sp}{'0'})) { $sp2nbc{$sp}{'0'}=0; }
    print INP "\t$sp\t$sp2nbc{$sp}{'2'}\t$sp2nbc{$sp}{'1'}\t$sp2nbc{$sp}{'0'}\n";
}
print INP "\n";
print "OK.\n";


# gather evidence from neighbours
print "Gathering evidence from neighbours ...... ";
undef my %gn2evidence;
foreach my $gn (sort keys %gene2nbrs) {
    if(scalar(@{$gene2nbrs{$gn}})>1) { next; }
    if(!defined($gn2og{$gn})) { next; }
    undef my %ognbsup;
    undef my %ognbspe;
    my $possible=0;
    my %pspecs;

    my $nb=${$gene2nbrs{$gn}}[0];
    
    print LOG ">Gene\tGn-OG\tNb\tNEIB-OG\n";
    if(defined($gn2og{$nb})) { print LOG "$gn\t$gn2og{$gn}\t$nb\t$gn2og{$nb}\n\n"; }
    else { print LOG "$gn\t$gn2og{$gn}\t$nb\tnoog\n\n"; }
    print LOG "Ortho\tNb1\tNb1-OG\tNEIB?\tNb2\tNb2-OG\tNEIB?\n";

    my @orthos=@{$og2gns{$gn2og{$gn}}};
    foreach my $or (@orthos) {
	if($or eq $gn) { next; }
	if($gn2sp{$or} eq $gn2sp{$gn}) { next; }

	print LOG "$or";
	foreach my $on (@{$gene2nbrs{$or}}) {
	    my $pair='no';
	    if(defined($gn2og{$on}) && defined($gn2og{$nb})) { 
		if($gn2og{$nb} eq $gn2og{$on}) { $pair='NEIB'; }
		if($gn2og{$gn} eq $gn2og{$on}) { $pair='PARA'; }
		print LOG "\t$on\t$gn2og{$on}\t$pair";
	    }
	    elsif(defined($gn2og{$on})) { 
		if($gn2og{$gn} eq $gn2og{$on}) { $pair='PARA'; }
		print LOG "\t$on\t$gn2og{$on}\t$pair"; 
	    }
	    else { print LOG "\t$on\tnoog\t$pair"; }
	    if($on ne 'none' && $pair ne 'PARA') { $possible++; }
	    if(defined($gn2og{$on})) {
		if($pair eq 'PARA') { next; }
		if(defined($ognbsup{"$gn2og{$gn}\:$gn2og{$on}"})) { $ognbsup{"$gn2og{$gn}\:$gn2og{$on}"}++; }
		else { $ognbsup{"$gn2og{$gn}\:$gn2og{$on}"}=1; }
		push(@{$ognbspe{"$gn2og{$gn}\:$gn2og{$on}"}},$gn2sp{$on});
		$pspecs{$gn2sp{$on}}=1;
	    }
	    else {
		if($on eq 'none') { next; }
		if(defined($ognbsup{"$gn2og{$gn}\:noog"})) { $ognbsup{"$gn2og{$gn}\:noog"}++; }
		else { $ognbsup{"$gn2og{$gn}\:noog"}=1; }
		push(@{$ognbspe{"$gn2og{$gn}\:noog"}},$gn2sp{$on});
		$pspecs{$gn2sp{$on}}=1;
	    }
	}
	if(scalar(@{$gene2nbrs{$or}})==1) { print LOG "\tnone\tnoog\tno\n"; }
	else { print LOG "\n"; }
    }
    print LOG "\n";

    undef my %pairorder;
    foreach my $ognb (sort keys %ognbsup) {
	undef my %specs;
	foreach my $spe (@{$ognbspe{$ognb}}) { $specs{$spe}=1; }
	push(@{$pairorder{$ognb}},scalar(keys %specs),$ognbsup{$ognb});
    }    
    foreach my $ognb (sort { $pairorder{$b}[0] <=> $pairorder{$a}[0] || $pairorder{$b}[1] <=> $pairorder{$a}[1] } keys %pairorder) {
	undef my %specs;
	foreach my $spe (@{$ognbspe{$ognb}}) { $specs{$spe}=1; }
	my($og1,$og2)=split(/:/,$ognb);
	my $match='OTHR';
	if(defined($gn2og{$nb})) {
	    if($og2 eq $gn2og{$nb}) { $match='NEIB'; }
	}
	print LOG "$ognb\t$ognbsup{$ognb} / $possible\t". scalar(keys %specs) . " / ". scalar(keys %pspecs) . "\t$match\n";
	if(scalar(keys %specs)>1 && $ognb!~/noog/) {
	    push(@{$gn2evidence{$gn}},"$ognb\t$ognbsup{$ognb}\:$possible\t". scalar(keys %specs) . ":". scalar(keys %pspecs) . "\t$match");
	}
    }
    print LOG "\n\n";
 }
close(LOG);
print "OK.\n";

# order the best evidence
open(LOG,">$run\_OrthoStitch.log2.evidence.txt") || die $!;
print "Sorting best evidence ................... ";
undef my %gn2nbev;
undef my %gn2glev;
undef my %gn2best;
undef my %pairscr;
foreach my $gn (sort keys %gn2evidence) {
    my $gnspe=$gn2sp{$gn};
    my @neib=qw(none na na);
    my @oths=();
    foreach my $ev (@{$gn2evidence{$gn}}) {
	if($ev=~/NEIB/) { @neib=split(/\t/,$ev); }
	else { push(@oths,$ev); }
    }
    $gn2nbev{$gn}="${$gene2nbrs{$gn}}[0]\t$neib[0]\t$neib[1]\t$neib[2]";
    my @samespecorth=();
    my $best=0;
    foreach my $othr (@oths) { 
	my @bits=split(/\t/,$othr);
	my @ogs=split(/:/,$bits[0]);
	foreach my $or (@{$og2gns{$ogs[1]}}) {
	    my $orspe=$gn2sp{$or};
	    if($gnspe eq $orspe) {
		if($gn2chr{$gn} eq $gn2chr{$or}) { next; }
		push(@samespecorth,$or); 
		if(defined($lonely{$or})) { 
		    push(@{$gn2glev{$gn}},"\t$bits[0]\t$bits[1]\t$bits[2]\t$or\[lonely]"); 
		}
		else { push(@{$gn2glev{$gn}},"\t$bits[0]\t$bits[1]\t$bits[2]\t$or"); }
		my ($ssp,$psp)=split(/:/,$bits[2]);
		my $score=$ssp/$psp;
		if($score>=$best) { 
		    if(defined($lonely{$or})) { push(@{$gn2best{$gn}},$or); }
		    $best=$score; 
		    $pairscr{"$gn:$or"}=$bits[2]; 
		}
	    }
	}
    }
    if(scalar(@samespecorth)>0) { 
	print LOG "$gn\t$gn2nbev{$gn}\n";
	foreach my $o (@{$gn2glev{$gn}}) { print LOG "$o\n"; } 
    }
}
print "OK.\n";
close(LOG);

# reciprocal evidence search
print "Searching for reciprocal best matches ... ";
undef my %brhs;
foreach my $gn (sort keys %gn2best) {
    foreach my $best (@{$gn2best{$gn}}) {
	if(defined(${$gn2best{$best}}[0])) {
	    foreach my $back (@{$gn2best{$best}}) {
		if($back eq $gn) { 
		    my $brh=join(":",sort($best,$back));
		    my $scr=join("..",sort($pairscr{"$gn:$best"},$pairscr{"$best:$back"}));
		    $brhs{$brh}=$scr;
		}
	    }
	}
    }
}
undef my %brhnbsup;
undef my %brhgroups;
undef my %seen;
undef my %removes;
my $bg=0;
foreach my $brh (sort keys %brhs) {
    my ($gn1,$gn2)=split(/:/,$brh);
    if(defined($seen{$gn1}) || defined($seen{$gn2})) {
	if(defined($seen{$gn1}) && defined($seen{$gn2})) {
	    if($seen{$gn1} != $seen{$gn2}) {
		$bg++;
		@{$brhgroups{$bg}}=(@{$brhgroups{$seen{$gn1}}},@{$brhgroups{$seen{$gn2}}},$brh);
		$removes{$seen{$gn1}}=1;;
		$removes{$seen{$gn2}}=1;
		$seen{$gn1}=$bg;
		$seen{$gn2}=$bg;
	    }
	    else {
		@{$brhgroups{$seen{$gn1}}}=(@{$brhgroups{$seen{$gn1}}},$brh);
	    }
	}
	elsif(defined($seen{$gn1}) && !defined($seen{$gn2})) {
	    @{$brhgroups{$seen{$gn1}}}=(@{$brhgroups{$seen{$gn1}}},$brh);
	    $seen{$gn2}=$seen{$gn1};
	}
	elsif(!defined($seen{$gn1}) && defined($seen{$gn2})) {
	    @{$brhgroups{$seen{$gn2}}}=(@{$brhgroups{$seen{$gn2}}},$brh);
	    $seen{$gn1}=$seen{$gn2};
	}
    }
    else { 
	$bg++;
	$seen{$gn1}=$bg;
	$seen{$gn2}=$bg;
	push(@{$brhgroups{$bg}},$brh);
    }
    my @gn1evs=split(/\t/,$gn2nbev{$gn1});
    my @gn2evs=split(/\t/,$gn2nbev{$gn2});
    my $nbsup=join("..",sort($gn1evs[3],$gn2evs[3]));
    $brhnbsup{$brh}=$nbsup;
}

open(LOG1,">$run\_OrthoStitch.log3.reciprocalpairs.txt") || die $!;
open(LOG2,">$run\_OrthoStitch.log4.reciprocalcsets.txt") || die $!;
undef my %species2pairs;
undef my %species2csets;
foreach my $grp ( sort { $brhgroups{$a} <=> $brhgroups{$b} } keys %brhgroups) {
    if(defined($removes{$grp})) { next; }
    if(scalar(@{$brhgroups{$grp}})>1) {
	foreach my $brh (@{$brhgroups{$grp}}) {
	    my ($g1,$g2)=split(/:/,$brh);
	    print LOG2 "$brh\t$brhs{$brh}\t$brhnbsup{$brh}\t$gn2chr{$g1}:$gn2chr{$g2}\t" . scalar(@{$brhgroups{$grp}}) . "\n";
	    my $spe=$gn2sp{$g1};
	    push(@{$species2csets{$spe}},$brh);
	}
    }
    else {
	my $brh=@{$brhgroups{$grp}}[0];
	my ($g1,$g2)=split(/:/,$brh);
	print LOG1 "$brh\t$brhs{$brh}\t$brhnbsup{$brh}\t$gn2chr{$g1}:$gn2chr{$g2}\n";
	my $spe=$gn2sp{$g1};
	push(@{$species2pairs{$spe}},$brh);
    }
}
close(LOG1);
close(LOG2);
print "OK.\n";

# print out simple pairs
print "Formatting adjacency results ............ ";
open(LOG1,">$run\_OrthoStitch.res1.simple_adjacencies_all.txt") || die $!;
print LOG1 "species\tctg1\tctg2\tctg1_orientation\tctg2_orientation\tctg1-ctg2_gap\tscore\tevidence\n";
undef my %brh2print;
undef my %simpleseen;
foreach my $sp (sort keys %ancsp) {
    foreach my $brh (@{$species2pairs{$sp}}) {
	my ($g1,$g2)=split(/:/,$brh);
	my @scaffs=sort($gn2chr{$g1},$gn2chr{$g2});
	my $scpr=join(":",@scaffs);
	push(@{$simpleseen{$scpr}},$brh);
	undef my %scaff2gn;
	$scaff2gn{$gn2chr{$g1}}=$g1;
	$scaff2gn{$gn2chr{$g2}}=$g2;
	undef my %gloc;
	my $totgenes=scalar(@{$chr2gns{"$sp:$gn2chr{$g1}"}});
	if($totgenes==1) { $gloc{$g1}='only'; }
	elsif( @{$chr2gns{"$sp:$gn2chr{$g1}"}}[0] eq $g1) { $gloc{$g1}='first'; }
	elsif( @{$chr2gns{"$sp:$gn2chr{$g1}"}}[$totgenes-1] eq $g1) { $gloc{$g1}='last'; }
	$totgenes=scalar(@{$chr2gns{"$sp:$gn2chr{$g2}"}});
	if($totgenes==1) { $gloc{$g2}='only'; }
	elsif( @{$chr2gns{"$sp:$gn2chr{$g2}"}}[0] eq $g2) { $gloc{$g2}='first'; }
	elsif( @{$chr2gns{"$sp:$gn2chr{$g2}"}}[$totgenes-1] eq $g2) { $gloc{$g2}='last'; }
	
	undef my %scaffdir;
	if($gloc{$scaff2gn{$scaffs[0]}} eq 'first') {
	    if($gloc{$scaff2gn{$scaffs[1]}} eq 'first') {
		$scaffdir{$scaffs[0]}='-';
		$scaffdir{$scaffs[1]}='+';
	    }
	    elsif($gloc{$scaff2gn{$scaffs[1]}} eq 'last') {
		$scaffdir{$scaffs[0]}='-';
		$scaffdir{$scaffs[1]}='-';
	    }
	    elsif($gloc{$scaff2gn{$scaffs[1]}} eq 'only') {
		$scaffdir{$scaffs[0]}='-';
		$scaffdir{$scaffs[1]}='?';	
	    }
	}
	elsif($gloc{$scaff2gn{$scaffs[0]}} eq 'last') {
	    if($gloc{$scaff2gn{$scaffs[1]}} eq 'first') {
		$scaffdir{$scaffs[0]}='+';
		$scaffdir{$scaffs[1]}='+';
	    }
	    elsif($gloc{$scaff2gn{$scaffs[1]}} eq 'last') {
		$scaffdir{$scaffs[0]}='+';
		$scaffdir{$scaffs[1]}='-';
	    }
	    elsif($gloc{$scaff2gn{$scaffs[1]}} eq 'only') {
		$scaffdir{$scaffs[0]}='+';
		$scaffdir{$scaffs[1]}='?';
	    }
	}
	elsif($gloc{$scaff2gn{$scaffs[0]}} eq 'only') {
	    if($gloc{$scaff2gn{$scaffs[1]}} eq 'first') {
		$scaffdir{$scaffs[0]}='?';
		$scaffdir{$scaffs[1]}='+';	
	    }
	    elsif($gloc{$scaff2gn{$scaffs[1]}} eq 'last') {
		$scaffdir{$scaffs[0]}='?';
		$scaffdir{$scaffs[1]}='-';
	    }
	    elsif($gloc{$scaff2gn{$scaffs[1]}} eq 'only') {
		$scaffdir{$scaffs[0]}='?';
		$scaffdir{$scaffs[1]}='?';
	    }
	}
	print LOG1 "$sp\t$scaffs[0]\t$scaffs[1]\t$scaffdir{$scaffs[0]}\t$scaffdir{$scaffs[1]}\t?\t";
	my ($glu1,$glu2)=split(/\.\./,$brhs{$brh});
	my ($msyn1,$psyn1)=split(/:/,$glu1);
	my ($msyn2,$psyn2)=split(/:/,$glu2);
	my $synscore = ( ($msyn1/$psyn1) + ($msyn2/$psyn2) ) / 2;
	my $universality = (($psyn1+$psyn2)/2) / (scalar(keys %ancsp)-1);
	my $synscore = sprintf("%.3f", $synscore*$universality );
	print LOG1 "$synscore\t";
	print LOG1 "$brhs{$brh}\-$brhnbsup{$brh};";
	print LOG1 "$scaff2gn{$scaffs[0]}\[$gloc{$scaff2gn{$scaffs[0]}}\]\.\.$scaff2gn{$scaffs[1]}\[$gloc{$scaff2gn{$scaffs[1]}}\]\n";
	$brh2print{$brh}="$sp\t$scaffs[0]\t$scaffs[1]\t$scaffdir{$scaffs[0]}\t$scaffdir{$scaffs[1]}\t?\t$synscore\t$brhs{$brh}\-$brhnbsup{$brh};$scaff2gn{$scaffs[0]}\[$gloc{$scaff2gn{$scaffs[0]}}\]\.\.$scaff2gn{$scaffs[1]}\[$gloc{$scaff2gn{$scaffs[1]}}\]";
    }
}
close(LOG1);
undef my %tokill;
foreach my $dbl (sort keys %simpleseen) {
    if(scalar(@{$simpleseen{$dbl}})>1) {
	my $maxscr=0;
	my $winner='';
	foreach my $brh (@{$simpleseen{$dbl}}) {
	    my $score=0;
	    my @bits1=split(/\t/,$brh2print{$brh});
	    my @bits2=split(/;/,$bits1[$#bits1]);
	    my @bits3=split(/\-/,$bits2[0]);
	    my @bits4a=split(/\.\./,$bits3[0]);
	    my @bits4b=split(/\.\./,$bits3[1]);
	    if($bits4a[0]=~/(\d+):\d+/) { $score=$score+$1; }
	    if($bits4a[1]=~/(\d+):\d+/) { $score=$score+$1; }
	    if($bits4b[0]=~/(\d+):\d+/) { $score=$score+$1; }
	    if($bits4b[1]=~/(\d+):\d+/) { $score=$score+$1; }
	    if($score>=$maxscr) { $winner=$brh; $maxscr=$score; }
	}
	foreach my $brh (@{$simpleseen{$dbl}}) {
	    if($brh ne $winner) { $tokill{$brh}=1; }
	}
    }
}
my %tmp=%brh2print;
undef my %brh2print;
foreach my $brh (sort keys %tmp) {
    if(!defined($tokill{$brh})) { $brh2print{$brh}=$tmp{$brh}; }
}
print "OK.\n";

# resolve complex sets
open(LOG1,">$run\_OrthoStitch.res2.complex_adjacencies_resolved.txt") || die $!;
print "Resolving complex adjacencies ........... ";
print LOG1 "species\tctg1\tctg2\tctg1_orientation\tctg2_orientation\tctg1-ctg2_gap\tscore\tevidence\n";

undef my %species2rsets;
foreach my $grp ( sort { $brhgroups{$a} <=> $brhgroups{$b} } keys %brhgroups) {
    if(defined($removes{$grp})) { next; }
    if(scalar(@{$brhgroups{$grp}})>1) {
	undef my %gncnt;
	undef my %gnloc;
	foreach my $brh (@{$brhgroups{$grp}}) {
	    my ($g1,$g2)=split(/:/,$brh);
	    my $spe=$gn2sp{$g1};
	    undef my %gloc;
	    my $sp=$gn2sp{$g1};
	    my $totgenes=scalar(@{$chr2gns{"$sp:$gn2chr{$g1}"}});
	    if($totgenes==1) { $gloc{$g1}='only'; }
	    elsif( @{$chr2gns{"$sp:$gn2chr{$g1}"}}[0] eq $g1) { $gloc{$g1}='first'; }
	    elsif( @{$chr2gns{"$sp:$gn2chr{$g1}"}}[$totgenes-1] eq $g1) { $gloc{$g1}='last'; }
	    $totgenes=scalar(@{$chr2gns{"$sp:$gn2chr{$g2}"}});
	    if($totgenes==1) { $gloc{$g2}='only'; }
	    elsif( @{$chr2gns{"$sp:$gn2chr{$g2}"}}[0] eq $g2) { $gloc{$g2}='first'; }
	    elsif( @{$chr2gns{"$sp:$gn2chr{$g2}"}}[$totgenes-1] eq $g2) { $gloc{$g2}='last'; }
	    if(defined($gncnt{$g1})) { $gncnt{$g1}++; }
	    else { $gncnt{$g1}=1; }
	    if(defined($gncnt{$g2})) { $gncnt{$g2}++; }
	    else { $gncnt{$g2}=1; }
	    $gnloc{$g1}=$gloc{$g1};
	    $gnloc{$g2}=$gloc{$g2};
	}
	my @firsts=();
	my @lasts=();
	my @onlys=();
	my $resolve='YES';
	foreach my $gn (sort keys %gncnt) {
	    if($gncnt{$gn}==1 && $gnloc{$gn} eq 'first') { push(@firsts,$gn); }
	    elsif($gncnt{$gn}==1 && $gnloc{$gn} eq 'last') { push(@lasts,$gn); }
	    elsif($gncnt{$gn}<=2 && $gnloc{$gn} eq 'only') { push(@onlys,$gn); }
	    elsif($gncnt{$gn}>1 && $gnloc{$gn} eq 'first') { $resolve='NO'; }
	    elsif($gncnt{$gn}>1 && $gnloc{$gn} eq 'last') { $resolve='NO'; }    # too strict! - reconsider later!
	    elsif($gncnt{$gn}>2 && $gnloc{$gn} eq 'only') { $resolve='NO'; }
	    foreach my $gnn (sort keys %gncnt) {
		if($gn2og{$gn} eq $gn2og{$gnn} && $gn2chr{$gn} ne $gn2chr{$gnn}) { $resolve='NO'; }
	    }
	}
	if(scalar(@firsts)==1 && scalar(@lasts)==1) {
	    if($gn2chr{$firsts[0]} eq $gn2chr{$lasts[0]}) { $resolve='NO'; }
	}
	my $mergeFirstLast=1;
	if($resolve eq 'YES' && scalar(@firsts)==1 && scalar(@lasts)==1) {
	    if($gn2og{$firsts[0]} eq $gn2og{$lasts[0]} && $gn2chr{$firsts[0]} eq $gn2chr{$lasts[0]}) { $mergeFirstLast=0; }
	}
	if($resolve eq 'YES') {
	    foreach my $brh (@{$brhgroups{$grp}}) {
		my ($g1,$g2)=split(/:/,$brh);
		my $spe=$gn2sp{$g1};
		my @scaffs=sort($gn2chr{$g1},$gn2chr{$g2});
		undef my %scaff2gn;
		$scaff2gn{$gn2chr{$g1}}=$g1;
		$scaff2gn{$gn2chr{$g2}}=$g2;
		push(@{$species2rsets{$spe}},$brh);

		undef my %scaffdir;
		if($gnloc{$scaff2gn{$scaffs[0]}} eq 'first') {
		    if($gnloc{$scaff2gn{$scaffs[1]}} eq 'first') {
			$scaffdir{$scaffs[0]}='-';
			$scaffdir{$scaffs[1]}='+';
		    }
		    elsif($gnloc{$scaff2gn{$scaffs[1]}} eq 'last') {
			$scaffdir{$scaffs[0]}='-';
			$scaffdir{$scaffs[1]}='-';
		    }
		    elsif($gnloc{$scaff2gn{$scaffs[1]}} eq 'only') {
			$scaffdir{$scaffs[0]}='-';
			$scaffdir{$scaffs[1]}='?';	
		    }
		}
		elsif($gnloc{$scaff2gn{$scaffs[0]}} eq 'last') {
		    if($gnloc{$scaff2gn{$scaffs[1]}} eq 'first') {
			$scaffdir{$scaffs[0]}='+';
			$scaffdir{$scaffs[1]}='+';
		    }
		    elsif($gnloc{$scaff2gn{$scaffs[1]}} eq 'last') {
			$scaffdir{$scaffs[0]}='+';
			$scaffdir{$scaffs[1]}='-';
		    }
		    elsif($gnloc{$scaff2gn{$scaffs[1]}} eq 'only') {
			$scaffdir{$scaffs[0]}='+';
			$scaffdir{$scaffs[1]}='?';
		    }
		}
		elsif($gnloc{$scaff2gn{$scaffs[0]}} eq 'only') {
		    if($gnloc{$scaff2gn{$scaffs[1]}} eq 'first') {
			$scaffdir{$scaffs[0]}='?';
			$scaffdir{$scaffs[1]}='+';	
		    }
		    elsif($gnloc{$scaff2gn{$scaffs[1]}} eq 'last') {
			$scaffdir{$scaffs[0]}='?';
			$scaffdir{$scaffs[1]}='-';
		    }
		    elsif($gnloc{$scaff2gn{$scaffs[1]}} eq 'only') {
			$scaffdir{$scaffs[0]}='?';
			$scaffdir{$scaffs[1]}='?';
		    }
		}
		my ($glu1,$glu2)=split(/\.\./,$brhs{$brh});
		my ($msyn1,$psyn1)=split(/:/,$glu1);
		my ($msyn2,$psyn2)=split(/:/,$glu2);
		my $synscore = ( ($msyn1/$psyn1) + ($msyn2/$psyn2) ) / 2;
		my $universality = (($psyn1+$psyn2)/2) / (scalar(keys %ancsp)-1);
		my $synscore = sprintf("%.3f", $synscore*$universality );
		
		if($mergeFirstLast==0) { 
		    print LOG1 "$brh\t$spe\t$scaffs[0]\t$scaffs[1]\t?\t?\t?\t";
		    print LOG1 "$synscore\t";
		    print LOG1 "$brhs{$brh}\-$brhnbsup{$brh};";
		    print LOG1 "$scaff2gn{$scaffs[0]}\[$gnloc{$scaff2gn{$scaffs[0]}}\]\.\.$scaff2gn{$scaffs[1]}\[$gnloc{$scaff2gn{$scaffs[1]}}\]\n";
		    $brh2print{$brh}="$spe\t$scaffs[0]\t$scaffs[1]\t$scaffdir{$scaffs[0]}\t$scaffdir{$scaffs[1]}\t?\t$synscore\t$brhs{$brh}\-$brhnbsup{$brh};$scaff2gn{$scaffs[0]}\[$gnloc{$scaff2gn{$scaffs[0]}}\]\.\.$scaff2gn{$scaffs[1]}\[$gnloc{$scaff2gn{$scaffs[1]}}\]";
		    last;
		}
		print LOG1 "$brh\t$spe\t$scaffs[0]\t$scaffs[1]\t$scaffdir{$scaffs[0]}\t$scaffdir{$scaffs[1]}\t?\t";
		print LOG1 "$synscore\t";
		print LOG1 "$brhs{$brh}\-$brhnbsup{$brh};";
		print LOG1 "$scaff2gn{$scaffs[0]}\[$gnloc{$scaff2gn{$scaffs[0]}}\]\.\.$scaff2gn{$scaffs[1]}\[$gnloc{$scaff2gn{$scaffs[1]}}\]\n";
		$brh2print{$brh}="$spe\t$scaffs[0]\t$scaffs[1]\t$scaffdir{$scaffs[0]}\t$scaffdir{$scaffs[1]}\t?\t$synscore\t$brhs{$brh}\-$brhnbsup{$brh};$scaff2gn{$scaffs[0]}\[$gnloc{$scaff2gn{$scaffs[0]}}\]\.\.$scaff2gn{$scaffs[1]}\[$gnloc{$scaff2gn{$scaffs[1]}}\]";
	    }
   	}
	else {
	    undef my %scafpairs;
	    undef my %scafpair2evidence;
	    undef my %scafends;
	    my $spe='';
	    foreach my $brh (@{$brhgroups{$grp}}) {
		my ($g1,$g2)=split(/:/,$brh);
		$spe=$gn2sp{$g1};
		my @scaffs=sort($gn2chr{$g1},$gn2chr{$g2});
		undef my %scaff2gn;
		$scaff2gn{$gn2chr{$g1}}=$g1;
		$scaff2gn{$gn2chr{$g2}}=$g2;
		push(@{$scafpair2evidence{"$scaffs[0]:$scaffs[1]"}},"$brhs{$brh}\-$brhnbsup{$brh};$scaff2gn{$scaffs[0]}\[$gnloc{$scaff2gn{$scaffs[0]}}\]\.\.$scaff2gn{$scaffs[1]}\[$gnloc{$scaff2gn{$scaffs[1]}}\]");
		push(@{$scafpairs{"$scaffs[0]:$scaffs[1]"}},$brh);
		push(@{$scafends{$scaffs[0]}},$gnloc{$scaff2gn{$scaffs[0]}});
		push(@{$scafends{$scaffs[1]}},$gnloc{$scaff2gn{$scaffs[1]}});
	    }
	    foreach my $scafpair (sort keys %scafpairs) {
		if(scalar(@{$scafpairs{$scafpair}})>1) {
		    my ($s0,$s1)=split(/:/,$scafpair);
		    my $s0ends=join("+",sort(@{$scafends{$s0}}));
		    my $s1ends=join("+",sort(@{$scafends{$s1}}));

		    if( $s0ends eq 'last+last' || $s0ends eq 'first+first' || $s0ends eq 'only+only' ) {
			if( $s1ends eq 'first+last' && scalar(@{$chr2gns{"$spe:$s1"}})==2 ) {
			    my $brh=${$scafpairs{$scafpair}}[0];
			    my $evi=${$scafpair2evidence{$scafpair}}[0];
			    my $dir='?';
			    if($s0ends=~/last/) { $dir='+'; }
			    elsif($s0ends=~/first/) { $dir='-'; }
			    
			    my ($glu1,$glu2)=split(/\.\./,$brhs{$brh});
			    my ($msyn1,$psyn1)=split(/:/,$glu1);
			    my ($msyn2,$psyn2)=split(/:/,$glu2);
			    my $synscore = ( ($msyn1/$psyn1) + ($msyn2/$psyn2) ) / 2;
			    my $universality = (($psyn1+$psyn2)/2) / (scalar(keys %ancsp)-1);
			    my $synscore = sprintf("%.3f", $synscore*$universality );
			    			    
			    print LOG1 "$brh\t$spe\t$s0\t$s1\t$dir\t?\t?\t$synscore\t$evi\n";
			    $brh2print{$brh}="$spe\t$s0\t$s1\t$dir\t?\t?\t$synscore\t$evi";
			    push(@{$species2rsets{$spe}},$brh);
			}
		    }
		    if( $s1ends eq 'last+last' || $s1ends eq 'first+first' || $s1ends eq 'only+only' ) {
			if( $s0ends eq 'first+last' && scalar(@{$chr2gns{"$spe:$s0"}})==2 ) {
			    my $brh=${$scafpairs{$scafpair}}[0];
			    my $evi=${$scafpair2evidence{$scafpair}}[0];
			    my $dir='?';
			    if($s1ends=~/last/) { $dir='-'; }
			    elsif($s1ends=~/first/) { $dir='+'; }

			    my ($glu1,$glu2)=split(/\.\./,$brhs{$brh});
			    my ($msyn1,$psyn1)=split(/:/,$glu1);
			    my ($msyn2,$psyn2)=split(/:/,$glu2);
			    my $synscore = ( ($msyn1/$psyn1) + ($msyn2/$psyn2) ) / 2;
			    my $universality = (($psyn1+$psyn2)/2) / (scalar(keys %ancsp)-1);
			    my $synscore = sprintf("%.3f", $synscore*$universality );
			    			    
			    print LOG1 "$brh\t$spe\t$s0\t$s1\t?\t$dir\t?\t$synscore\t$evi\n";
			    $brh2print{$brh}="$spe\t$s0\t$s1\t?\t$dir\t?\t$synscore\t$evi";
			    push(@{$species2rsets{$spe}},$brh);
			}
		    }

		    
		}
	    }
	}
    }
}
close(LOG1);
print "OK.\n";

# find and order multi-scaffold sets
print "Building multi-scaffold sets ............ ";
open(LOG1,">$run\_OrthoStitch.log5.multiadjacencies.txt") || die $!;
open(LOG2,">$run\_OrthoStitch.res3.multi-adjacencies.txt") || die $!;
print LOG2 "# GRIMM-format multi-adjacencies\n";
undef my %multinonresolve;
foreach my $sp (sort keys %ancsp) {
    undef my %scaffgroups;
    undef my %seen;
    my $bg=0;
    undef my %removes;
    my @brh_pairs_rsets=();
    if(defined(${$species2pairs{$sp}}[0])) { push(@brh_pairs_rsets,@{$species2pairs{$sp}});}
    if(defined(${$species2rsets{$sp}}[0])) { push(@brh_pairs_rsets,@{$species2rsets{$sp}}); }
    if(scalar(@brh_pairs_rsets)>0) { print LOG2 ">$sp\n"; }
    foreach my $brh ( sort (@brh_pairs_rsets) ) {
	if(!defined($brh2print{$brh})) { next; }
	my @scaffs=split(/\t/,$brh2print{$brh});
	if(defined($seen{$scaffs[1]}) || defined($seen{$scaffs[2]})) {
	    if(defined($seen{$scaffs[1]}) && defined($seen{$scaffs[2]})) {
		if($seen{$scaffs[1]} != $seen{$scaffs[2]}) {
		    $bg++;
		    @{$scaffgroups{$bg}}=(@{$scaffgroups{$seen{$scaffs[1]}}},@{$scaffgroups{$seen{$scaffs[2]}}},$brh);
		    $removes{$seen{$scaffs[1]}}=1;
		    $removes{$seen{$scaffs[2]}}=1;
		    $seen{$scaffs[1]}=$bg;
		    $seen{$scaffs[2]}=$bg;
		}
		else {
		    @{$scaffgroups{$seen{$scaffs[1]}}}=(@{$scaffgroups{$seen{$scaffs[1]}}},$brh);
		}
	    }
	    if(defined($seen{$scaffs[1]}) && !defined($seen{$scaffs[2]})) {
		@{$scaffgroups{$seen{$scaffs[1]}}}=(@{$scaffgroups{$seen{$scaffs[1]}}},$brh);
		$seen{$scaffs[2]}=$seen{$scaffs[1]};
	    }
	    if(!defined($seen{$scaffs[1]}) && defined($seen{$scaffs[2]})) {
		@{$scaffgroups{$seen{$scaffs[2]}}}=(@{$scaffgroups{$seen{$scaffs[2]}}},$brh);
		$seen{$scaffs[1]}=$seen{$scaffs[2]};
	    }
	}
	else { 
	    $bg++;
	    $seen{$scaffs[1]}=$bg;
	    $seen{$scaffs[2]}=$bg;
	    push(@{$scaffgroups{$bg}},$brh);
	}
    }
    foreach my $scaffgroup (sort { $a<=>$b } keys %scaffgroups) {
	if(defined($removes{$scaffgroup})) { next; }
	undef my %pair2directions;
	if(scalar(@{$scaffgroups{$scaffgroup}})>1) {
	    print LOG1 "$scaffgroup\t" . scalar(@{$scaffgroups{$scaffgroup}}) . " pairs\n";
	    undef my %scaff2up;
	    undef my %scaff2dn;
	    undef my %uniscaff;
	    foreach my $brh (sort @{$scaffgroups{$scaffgroup}}) {
		my @scaffs=split(/\t/,$brh2print{$brh});
		if(defined($uniscaff{$scaffs[1]})) { $uniscaff{$scaffs[1]}++; }
		else  { $uniscaff{$scaffs[1]}=1; }
		if(defined($uniscaff{$scaffs[2]})) { $uniscaff{$scaffs[2]}++; }
		else  { $uniscaff{$scaffs[2]}=1; }
		if($scaffs[3] eq '+') { $scaff2up{$scaffs[1]}=$scaffs[2]; }
		elsif($scaffs[3] eq '-') { $scaff2dn{$scaffs[1]}=$scaffs[2]; }
		else { 
		    if($scaffs[4] eq '+' && !defined($scaff2up{$scaffs[1]})) { $scaff2up{$scaffs[1]}=$scaffs[2]; }
		    elsif($scaffs[4] eq '-' && !defined($scaff2dn{$scaffs[1]})) { $scaff2dn{$scaffs[1]}=$scaffs[2]; }
		    elsif(!defined($scaff2up{$scaffs[1]})) { $scaff2up{$scaffs[1]}=$scaffs[2]; }
		    elsif(!defined($scaff2dn{$scaffs[1]})) { $scaff2dn{$scaffs[1]}=$scaffs[2]; }
		}
		if($scaffs[4] eq '+') { $scaff2dn{$scaffs[2]}=$scaffs[1]; }
		elsif($scaffs[4] eq '-') { $scaff2up{$scaffs[2]}=$scaffs[1]; }
		else { 
		    if($scaffs[3] eq '-' && !defined($scaff2up{$scaffs[2]})) { $scaff2up{$scaffs[2]}=$scaffs[1]; }
		    elsif($scaffs[3] eq '+' && !defined($scaff2dn{$scaffs[2]})) { $scaff2dn{$scaffs[2]}=$scaffs[1]; }
		    elsif(!defined($scaff2up{$scaffs[2]})) { $scaff2up{$scaffs[2]}=$scaffs[1]; }
		    elsif(!defined($scaff2dn{$scaffs[2]})) { $scaff2dn{$scaffs[2]}=$scaffs[1]; }
		}
		print LOG1 "\t$brh2print{$brh}\n";
		$pair2directions{"$scaffs[1]\t$scaffs[2]"}="$scaffs[3]\t$scaffs[4]";
	    }
	    my $ones=0;
	    my $twos=0;
	    my $bads=0;
	    my @starters=();
	    undef my %nonstarters;
	    foreach my $scaff (sort { $uniscaff{$a}<=>$uniscaff{$b} || $a cmp $b } keys %uniscaff) {
		print LOG1 "$uniscaff{$scaff}\t$scaff2dn{$scaff}\t$scaff\t$scaff2up{$scaff}\t";
		if($uniscaff{$scaff}==1) { $ones++; push(@starters,$scaff); }
		if($uniscaff{$scaff}==2) { $twos++; $nonstarters{$scaff}=1; }
		if($uniscaff{$scaff}>2) { $bads++; }
		print LOG1 "$scaffgroup\t$ones\t$twos\t$bads\n";
	    }
	    my ($st,$nd)=sort(@starters);
	    print LOG1 "START with $st end with $nd\n";
	    if($ones!=2 || $bads>0) { 
		print LOG1 "CANNOT RESOLVE:\t$sp\t$scaffgroup\t$ones\t$twos\t$bads\n\n";
		foreach my $brh (@{$scaffgroups{$scaffgroup}}) { $multinonresolve{$brh}=1; }
		next;
	    }

	    my @allordori=();
	    if(defined($st) && defined($nd)) {
		my @allordered=();
		push(@allordered,$st);
		my $focus=$st;
		undef my %usedneighbour;
		for (my $nonst=1; $nonst<=scalar(keys %nonstarters); $nonst++) {
		    $usedneighbour{$focus}=1;
		    my $nextup='';
		    if(defined($scaff2up{$focus})) { 
			if(!defined($usedneighbour{$scaff2up{$focus}})) { $nextup=$scaff2up{$focus}; }
		    }
		    if($nextup eq '') {
			if(defined($scaff2dn{$focus})) { 
			    if(!defined($usedneighbour{$scaff2dn{$focus}})) { $nextup=$scaff2dn{$focus}; }
			}
		    }
		    push(@allordered,$nextup);
		    $focus=$nextup;
		}
		push(@allordered,$nd);
		for (my $ord=0; $ord<$#allordered; $ord++) {
		    my $ord1=$ord+1;
		    if(defined($pair2directions{"$allordered[$ord]\t$allordered[$ord1]"})) {
			print LOG1 "$allordered[$ord]\t$allordered[$ord1]\tSAME\t" . $pair2directions{"$allordered[$ord]\t$allordered[$ord1]"} . "\n";
			my($oriA,$oriB)=split(/\t/,$pair2directions{"$allordered[$ord]\t$allordered[$ord1]"});
			push(@allordori,$oriA.$allordered[$ord],$oriB.$allordered[$ord1]);
		    }
		    else {
			print LOG1 "$allordered[$ord]\t$allordered[$ord1]\tREVE\t" . $pair2directions{"$allordered[$ord1]\t$allordered[$ord]"} . "\n";
			my($oriA,$oriB)=split(/\t/,$pair2directions{"$allordered[$ord1]\t$allordered[$ord]"});
			if($oriA eq '+') { $oriA='-'; }
			elsif($oriA eq '-') { $oriA='+'; }
			if($oriB eq '+') { $oriB='-'; }
			elsif($oriB eq '-') { $oriB='+'; }
			push(@allordori,$oriB.$allordered[$ord],$oriA.$allordered[$ord1]);
		    }
		}
	    }
	    else { print LOG1 "NO START OR END DEFINED\n"; }
	    print LOG1 join(" ",@allordori) . "\n\n";
	    my @ordori=();
	    push(@ordori,$allordori[0]);
	    for (my $scaffA=1; $scaffA<$#allordori; $scaffA+=2) {
		my $scaffB=$scaffA+1;
		if($scaffB==$#allordori) {next;}
		if($allordori[$scaffA] eq $allordori[$scaffB]) { push(@ordori,$allordori[$scaffA]); }
		else { print LOG1 "Error: " . join(" ",@allordori) . "\n\n"; }
	    }
    	    push(@ordori,$allordori[$#allordori]);
	    print LOG2 join(" ",@ordori) . "\n";
	}
	else {
	    my @brhdata=split(/\t/,$brh2print{${$scaffgroups{$scaffgroup}}[0]});
	    print LOG2 $brhdata[3] . $brhdata[1] . " " . $brhdata[4] . $brhdata[2] . "\n";
	}
    }
}
close(LOG1);
close(LOG2);
print "OK.\n";

print "Collating preliminary results ........... ";
open(LOG1,">$run\_OrthoStitch.res4.adjacencies_all.txt") || die $!;
print LOG1 "species\tctg1\tctg2\tctg1_orientation\tctg2_orientation\tctg1-ctg2_gap\tscore\tevidence\n";
open(LOG2,">$run\_OrthoStitch.res5.adjacencies_confident.txt") || die $!;
print LOG2 "species\tctg1\tctg2\tctg1_orientation\tctg2_orientation\tctg1-ctg2_gap\tscore\tevidence\n";
open(LOG3,">$run\_OrthoStitch.res6.adjacencies_superconfident.txt") || die $!;
print LOG3 "species\tctg1\tctg2\tctg1_orientation\tctg2_orientation\tctg1-ctg2_gap\tscore\tevidence\n";
open(LOG4,">$run\_OrthoStitch.res7.adjacencies_evidencemapped.txt") || die $!;
undef my %species2confi;
undef my %species2super;
undef my %doubles;

foreach my $sp (sort keys %ancsp) {
    my @brh_pairs_rsets=();
    if(defined(${$species2pairs{$sp}}[0]) && defined(${$species2rsets{$sp}}[0])) { push(@brh_pairs_rsets,@{$species2pairs{$sp}},@{$species2rsets{$sp}}); }
    elsif(defined(${$species2pairs{$sp}}[0]) && !defined(${$species2rsets{$sp}}[0])) { push(@brh_pairs_rsets,@{$species2pairs{$sp}}); }
    elsif(!defined(${$species2pairs{$sp}}[0]) && defined(${$species2rsets{$sp}}[0])) { push(@brh_pairs_rsets,@{$species2rsets{$sp}}); }
    else { next; }
    foreach my $brh ( sort (@brh_pairs_rsets) ) {
	if(!defined($brh2print{$brh})) { next; }
	if(defined($multinonresolve{$brh})) { next; }
	print LOG1 "$brh2print{$brh}\n";
	
	print LOG4 "\>$brh2print{$brh}\n";
	my @bits=split(/\t/,$brh2print{$brh});
	my ($score,$anchors)=split(/;/,$bits[$#bits]);
	my ($anchor1,$anchor2)=split(/\.\./,$anchors);
	my ($gn1,$re1)=split(/\[/,$anchor1);
	my ($gn2,$re2)=split(/\[/,$anchor2);
	my $dubcheck=join(':',$gn2sp{$gn1},sort($gn2chr{$gn1},$gn2chr{$gn2}));
	push(@{$doubles{$dubcheck}},$brh);

	print LOG4 "$sp\t";
	print LOG4 "$gn2chr{$gn1}\[$bits[3]\]\t";
	print LOG4 "$gn1";
	my $numnb=scalar(@{$gene2nbrs{$gn1}});
	if(${$gene2nbrs{$gn1}}[0] eq 'none') { $numnb--; }
	print LOG4 "\[$gn2og{$gn1}\]$numnb\t";
	if($bits[3] eq '+' && $bits[4] eq '+') { print LOG4 "---x>+-x-->\t"; }
	elsif($bits[3] eq '+' && $bits[4] eq '-') { print LOG4 "---x>+<x---\t"; }
	elsif($bits[3] eq '+' && $bits[4] eq '?') { print LOG4 "---x>+-x---\t"; }
	elsif($bits[3] eq '-' && $bits[4] eq '-') { print LOG4 "<--x-+<x---\t"; }
	elsif($bits[3] eq '-' && $bits[4] eq '+') { print LOG4 "<--x-+-x-->\t"; }
	elsif($bits[3] eq '-' && $bits[4] eq '?') { print LOG4 "<--x-+-x---\t"; }
	elsif($bits[3] eq '?' && $bits[4] eq '?') { print LOG4 "---x-+-x---\t"; }
	elsif($bits[3] eq '?' && $bits[4] eq '+') { print LOG4 "---x-+-x-->\t"; }
	elsif($bits[3] eq '?' && $bits[4] eq '-') { print LOG4 "---x-+<x---\t"; }
	print LOG4 "$gn2";
	$numnb=scalar(@{$gene2nbrs{$gn2}});
	if(${$gene2nbrs{$gn2}}[0] eq 'none') { $numnb--; }
	print LOG4 "\[$gn2og{$gn2}\]$numnb\t";
	print LOG4 "$gn2chr{$gn2}\[$bits[4]\]\n";

	foreach my $spe (sort keys %ancsp) {
	    if($spe eq $sp) { next; }
	    my @linkers1=();
	    foreach my $g (@{$og2gns{$gn2og{$gn1}}}) {
		if($gn2sp{$g} eq $spe) { push(@linkers1,$g); }
	    }
	    if(scalar(@linkers1)==0) { push(@linkers1,'noortho'); }
	    my @linkers2=();
	    foreach my $g (@{$og2gns{$gn2og{$gn2}}}) {
		if($gn2sp{$g} eq $spe) { push(@linkers2,$g); }
	    }
	    if(scalar(@linkers2)==0) { push(@linkers2,'noortho'); }
	    foreach my $ln1 (@linkers1) {
		foreach my $ln2 (@linkers2) {
		    my $og1='NOOG';
		    my $og2='NOOG';
		    my $chr1='na';
		    my $chr2='na';
		    if(defined($gn2og{$ln1})) { $og1=$gn2og{$ln1}; }
		    if(defined($gn2og{$ln2})) { $og2=$gn2og{$ln2}; }
		    if(defined($gn2chr{$ln1})) { $chr1=$gn2chr{$ln1}; }
		    if(defined($gn2chr{$ln2})) { $chr2=$gn2chr{$ln2}; }
		    my $joiner='';
		    my $neib=1;
		    foreach my $pnb (@{$gene2nbrs{$ln1}}) {
			if($pnb eq $ln2) { $neib=0; }
		    }

		    if($chr1 eq $chr2 && $chr1 ne 'na' && $neib==0) { $joiner='---x---x---'; }
		    elsif($chr1 eq $chr2 && $chr1 ne 'na' && $neib==1) { $joiner='--x--o--x--'; }
		    elsif($chr1 ne $chr2 && $chr1 ne 'na' && $chr2 ne 'na') { $joiner='-x--   --x-'; }
		    elsif($chr1 ne $chr2 && $chr1 eq 'na') { $joiner='....   --x-'; }
		    elsif($chr1 ne $chr2 && $chr2 eq 'na') { $joiner='-x--   ....'; }
		    elsif($chr1 eq 'na' && $chr2 eq 'na') { $joiner='....   ....'; }
		    my $numnb='';
		    if($ln1 ne 'noortho') {
			$numnb=scalar(@{$gene2nbrs{$ln1}});
			if(${$gene2nbrs{$ln1}}[0] eq 'none') { $numnb--; }
		    }
		    print LOG4 "$spe\t$chr1\t$ln1\[$og1\]$numnb\t$joiner\t";
		    $numnb='';
		    if($ln2 ne 'noortho') {
			$numnb=scalar(@{$gene2nbrs{$ln2}});
			if(${$gene2nbrs{$ln2}}[0] eq 'none') { $numnb--; }
		    }
		    print LOG4 "$ln2\[$og2\]$numnb\t$chr2\n";
		}
	    }
	}
	print LOG4 "\n";

	my ($glu1,$glu2)=split(/\.\./,$brhs{$brh});
	my ($msyn,$psyn)=split(/:/,$glu1);
	if($psyn<(scalar(keys %ancsp)/3)) { next; }
	elsif($msyn<($psyn/3)) { next; }
	($msyn,$psyn)=split(/:/,$glu2);
	if($psyn<(scalar(keys %ancsp)/3)) { next; }
	elsif($msyn<($psyn/3)) { next; }
	print LOG2 "$brh2print{$brh}\n";
	push(@{$species2confi{$sp}},$brh);
	if($brhnbsup{$brh}=~/na/) { next; }
	my ($nei1,$nei2)=split(/\.\./,$brhnbsup{$brh});
	($msyn,$psyn)=split(/:/,$nei1);
	if($psyn<(scalar(keys %ancsp)/3)) { next; }
	elsif($msyn<($psyn/3)) { next; }
	($msyn,$psyn)=split(/:/,$nei2);
	if($psyn<(scalar(keys %ancsp)/3)) { next; }
	elsif($msyn<($psyn/3)) { next; }
	print LOG3 "$brh2print{$brh}\n";
	push(@{$species2super{$sp}},$brh);
	

    }
}
close(LOG1);
close(LOG2);
close(LOG3);
close(LOG4);
print "OK.\n";

print INP "# 5. Possible adjacencies\n";
print INP "Species ID, pairs, complex, resolved, all, confident, superconfident\n";
foreach my $sp (sort keys %ancsp) {
    my $pairs=0;
    my $csets=0;
    my $rsets=0;
    my $confi=0;
    my $super=0;
    if(defined(${$species2pairs{$sp}}[0])) { $pairs=scalar(@{$species2pairs{$sp}}); }
    if(defined(${$species2csets{$sp}}[0])) { $csets=scalar(@{$species2csets{$sp}}); }
    if(defined(${$species2rsets{$sp}}[0])) { $rsets=scalar(@{$species2rsets{$sp}}); }
    if(defined(${$species2confi{$sp}}[0])) { $confi=scalar(@{$species2confi{$sp}}); }
    if(defined(${$species2super{$sp}}[0])) { $super=scalar(@{$species2super{$sp}}); }
    my $alls=$pairs+$rsets;
    print INP "\t$sp\t$pairs\t$csets\t$rsets\t$alls\t$confi\t$super\n";
}


# check for any doubles:
foreach my $double (sort keys %doubles) {
    if(scalar(@{$doubles{$double}})>1) {
	print $double . "\n";
	foreach my $brh (@{$doubles{$double}}) {
	    print "\t$brh\t$brhs{$brh}\t$brhnbsup{$brh}\t$brh2print{$brh}\n";
	}
	print "\n";
    }
}

# second round to try to improve orientation of unknowns
print "Checking for conflicts .................. ";
open(LOG1,">$run\_OrthoStitch.log6.orientations.txt") || die $!;
undef my %orientated;
if(-f "$run\_OrthoStitch.res7.adjacencies_evidencemapped.txt") {
    open(IN,"$run\_OrthoStitch.res7.adjacencies_evidencemapped.txt") || die $!;
    my @alladj=<IN>;
    close(IN);
    my $allfile=join(/\n/,@alladj);
    my @alladj=split(/\n>/,$allfile);
    foreach my $adjblock (@alladj) {
	my @adjlines=split(/\n/,$adjblock);
	my $adj=shift(@adjlines);
	$adj=~s/^>//;
	my @adjinfo=split(/\t/,$adj);
	if($adjinfo[3] eq '?' || $adjinfo[4] eq '?') {
	    print LOG1 "$adj\n";
	    my $adjgen=shift(@adjlines);
	    my @adjgeninfo=split(/\t/,$adjgen);
	    my $gen1='';
	    if($adjgeninfo[2]=~/(\S+)\[\S+\]\S/) { $gen1=$1; }
	    my $gen2='';
	    if($adjgeninfo[4]=~/(\S+)\[\S+\]\S/) { $gen2=$1; }
	    print LOG1 "$gen1\t$gn2dir{$gen1}\t$gen2\t$gn2dir{$gen2}\n";
	    my %adj2collinears;
	    my $sam=0;
	    my $dif=0;
	    foreach my $adjline (@adjlines) {
		chomp($adjline);
		my $collin='';
		if($adjline=~/\s+---x---x---\s+/) {
		    push(@{$adj2collinears{$adj}},$adjline);
		    my @colgeninfo=split(/\t/,$adjline);
		    my $gn1='';
		    if($colgeninfo[2]=~/(\S+)\[\S+\]\S/) { $gn1=$1; }
		    my $gn2='';
		    if($colgeninfo[4]=~/(\S+)\[\S+\]\S/) { $gn2=$1; }
		    print LOG1 "$gn1\t$gn2dir{$gn1}\t$gn2\t$gn2dir{$gn2}\t";
		    if($gn2dir{$gn1} eq $gn2dir{$gn2}) { print LOG1 "SAME\n"; $sam++; }
		    else { print LOG1 "DIFF\n"; $dif++; }
		}
	    }
	    my $totsamdif=$sam+$dif;
	    if($totsamdif<5) { print LOG1 "Not enough collinear orthologues to make a call\n"; }
	    else {
		my $samdif=sprintf("%.3f",$sam/$totsamdif);
		my $difsam=sprintf("%.3f",$dif/$totsamdif);
		if($samdif>0.85) { $totsamdif='SAM'; }
		elsif($difsam>0.85) { $totsamdif='DIF'; }
		else { $totsamdif='NON'; }
		print LOG1 "SAM: $sam\t$samdif\tDIF: $dif\t$difsam\tWinner: $totsamdif\n";

		if($totsamdif ne 'NON') {
		    my $gen1str='+';
		    my $gen2str='+';
		    
		    # different possible scenarios
		    if($adjinfo[3] eq '+' && $adjinfo[4] eq '?') {
			if($totsamdif eq 'SAM') {
			    if($gn2dir{$gen1} ne $gn2dir{$gen2}) { $gen2str='-'; }
			}
			elsif($totsamdif eq 'DIF') {
			    if($gn2dir{$gen1} eq $gn2dir{$gen2}) { $gen2str='-'; }
			}
		    }
		    elsif($adjinfo[3] eq '-' && $adjinfo[4] eq '?') {
			$gen1str='-';
			if($totsamdif eq 'SAM') {
			    if($gn2dir{$gen1} eq $gn2dir{$gen2}) { $gen2str='-'; }
			}
			elsif($totsamdif eq 'DIF') {
			    if($gn2dir{$gen1} ne $gn2dir{$gen2}) { $gen2str='-'; }
			}
		    }
		    elsif($adjinfo[3] eq '?' && $adjinfo[4] eq '+') {
			if($totsamdif eq 'SAM') {
			    if($gn2dir{$gen1} ne $gn2dir{$gen2}) { $gen1str='-'; }
			}
			elsif($totsamdif eq 'DIF') {
			    if($gn2dir{$gen1} eq $gn2dir{$gen2}) { $gen1str='-'; }
			}
		    }
		    elsif($adjinfo[3] eq '?' && $adjinfo[4] eq '-') {
			$gen2str='-';
			if($totsamdif eq 'SAM') {
			    if($gn2dir{$gen1} eq $gn2dir{$gen2}) { $gen1str='-'; }
			}
			elsif($totsamdif eq 'DIF') {
			    if($gn2dir{$gen1} ne $gn2dir{$gen2}) { $gen1str='-'; }
			}
		    }
		    elsif($adjinfo[3] eq '?' && $adjinfo[4] eq '?') {
			if($totsamdif eq 'SAM') {
			    if($gn2dir{$gen1} ne $gn2dir{$gen2}) { $gen2str='-'; }
			}
			elsif($totsamdif eq 'DIF') {
			    if($gn2dir{$gen1} eq $gn2dir{$gen2}) { $gen2str='-'; }
			}
		    }
		    print LOG1 "$adjinfo[0]\t$adjinfo[1]\t$adjinfo[2]\t$gen1str\t$gen2str\n";
		    $orientated{"$adjinfo[0]\t$adjinfo[1]\t$adjinfo[2]"}="$gen1str\t$gen2str";
		}
	    }
	    print LOG1 "\n";
	}
    }
}
else {
    print "FALIED to enhance orientations\n";
}

undef my %spec2finalpairs;
undef my %spec2finalconfi;
undef my %spec2finalsucon;
if(scalar(keys %orientated)>0) {
    undef my %all_conflicts;
    print LOG1 "\n\n=====\nORIENTATIONS UPDATED:\n\n";
    open(IN,"$run\_OrthoStitch.res3.multi-adjacencies.txt") || die $!;
    my @lines=<IN>;
    close(IN);
    open(OUT,">$run\_OrthoStitch.res3.1.multi-adjacencies-ori.txt") || die $!;
    open(LOG,">$run\_OrthoStitch.res3.1.multi-adjacencies-conflicts.txt") || die $!;
    my $spec='';
    foreach my $line (@lines) {
	chomp($line);
	if($line=~/^#/) { print OUT "$line\n"; next; }
	elsif($line=~/^>(\S+)/) { $spec=$1; print OUT "$line\n"; next; }
	else {
	    if($line=~/\?/) {
		print LOG1 "\n$line\n";
		my @scaffs=split(/\s+/,$line);
		my @scaffs_orig=split(/\s+/,$line);
		my @scafpairs=();
		undef my %conflicts;
		undef my %conflicts2;
		my $prevupdate='';
		for (my $i=0; $i<$#scaffs; $i++) {
		    my $j=$i+1;
		    my $scaf1=$scaffs[$i];
		    my $scaf2=$scaffs[$j];
		    if($scaf1=~/\?/ || $scaf2=~/\?/ || $prevupdate eq '?') {
			$prevupdate='';
			my $scaf1dir=substr($scaf1,0,1);
			my $scaf2dir=substr($scaf2,0,1);
			$scaf1=~s/^.//;
			$scaf2=~s/^.//;
			print LOG1 "\t$spec\t$scaf1\t$scaf2";
			if(defined($orientated{"$spec\t$scaf1\t$scaf2"})) { 
			    print LOG1 "\tFWD\t" . $orientated{"$spec\t$scaf1\t$scaf2"} . "\n";
			    my($sd1,$sd2)=split(/\t/,$orientated{"$spec\t$scaf1\t$scaf2"});
			    if(defined($conflicts2{$scaf1}) && $conflicts2{$scaf1} ne $sd1) {
				print LOG "$spec\t$scaf1\t$scaf2\tFWD\t" . $orientated{"$spec\t$scaf1\t$scaf2"} ."\n";
				print LOG "CONFLICT: $spec\t$scaf1 $scaf1dir $sd1\t$scaf2 $scaf2dir $sd2\n"; 
				$conflicts{$scaffs[$i]}=$i;
				$conflicts2{$scaf2}=$sd2;
				if($scaf2dir eq '?' && $sd2 ne '?') { $scaffs[$j]=$sd2 . $scaf2; }
				push(@scafpairs, $scaffs[$i], $scaffs[$j]);
			    }
			    elsif($scaf1dir eq $sd1) { push(@scafpairs, $sd1 . $scaf1, $sd2 . $scaf2); }
			    elsif($scaf2dir eq $sd2) { push(@scafpairs, $sd1 . $scaf1, $sd2 . $scaf2); }
			    elsif($scaf1dir eq '?' && $scaf2dir eq '?') { push(@scafpairs, $sd1 . $scaf1, $sd2 . $scaf2); }
			    else { 
				print LOG "$line\n";
				print LOG "$spec\t$scaf1\t$scaf2\tFWD\t"  . $orientated{"$spec\t$scaf1\t$scaf2"} . "\n";
				print LOG "CONFLICT: $spec\t$scaf1 $scaf1dir $sd1\t$scaf2 $scaf2dir $sd2\n";
				$conflicts{$scaffs[$i]}=$i;
				$conflicts2{$scaf2}=$sd2;
				if($scaf2dir eq '?' && $sd2 ne '?') { $scaffs[$j]=$sd2 . $scaf2; }
				push(@scafpairs, $scaffs[$i], $scaffs[$j]);
			    }
			    if($scaf2dir eq '?' && $sd2 ne '?') { $scaffs[$j]=$sd2 . $scaf2; $prevupdate='?'; }
			}
			elsif(defined($orientated{"$spec\t$scaf2\t$scaf1"})) { 
			    print LOG1 "\tREV\t" . $orientated{"$spec\t$scaf2\t$scaf1"} ."\n";
			    my($sd2,$sd1)=split(/\t/,$orientated{"$spec\t$scaf2\t$scaf1"});
			    if($sd1 eq '+') { $sd1='-'; }
			    elsif($sd1 eq '-') { $sd1='+'; }
			    if($sd2 eq '+') { $sd2='-'; }
			    elsif($sd2 eq '-') { $sd2='+'; }
			    if(defined($conflicts2{$scaf1}) && $conflicts2{$scaf1} ne $sd1) {
				print LOG "$spec\t$scaf1\t$scaf2\tREV\t" . $orientated{"$spec\t$scaf2\t$scaf1"} ."\n";
				print LOG "CONFLICT: $spec\t$scaf1 $scaf1dir $sd1\t$scaf2 $scaf2dir $sd2\n"; 
				$conflicts{$scaffs[$i]}=$i;
				$conflicts2{$scaf2}=$sd2;
				if($scaf2dir eq '?' && $sd2 ne '?') { $scaffs[$j]=$sd2 . $scaf2; }
				push(@scafpairs, $scaffs[$i], $scaffs[$j]);
			    }
			    elsif($scaf1dir eq $sd1) { push(@scafpairs, $sd1 . $scaf1, $sd2 . $scaf2); }
			    elsif($scaf2dir eq $sd2) { push(@scafpairs, $sd1 . $scaf1, $sd2 . $scaf2); }
			    elsif($scaf1dir eq '?' && $scaf2dir eq '?') { push(@scafpairs, $sd1 . $scaf1, $sd2 . $scaf2); }
			    else { 
				print LOG "$line\n";
				print LOG "$spec\t$scaf1\t$scaf2\tREV\t" . $orientated{"$spec\t$scaf2\t$scaf1"} ."\n";
				print LOG "CONFLICT: $spec\t$scaf1 $scaf1dir $sd1\t$scaf2 $scaf2dir $sd2\n"; 
				$conflicts{$scaffs[$i]}=$i;
				$conflicts2{$scaf2}=$sd2;
				if($scaf2dir eq '?' && $sd2 ne '?') { $scaffs[$j]=$sd2 . $scaf2; }
				push(@scafpairs, $scaffs[$i], $scaffs[$j]);
			    }
			    if($scaf2dir eq '?' && $sd2 ne '?') { $scaffs[$j]=$sd2 . $scaf2; $prevupdate='?'; }
			}
			else { 
			    print LOG1 "\tnone\n";
			    push(@scafpairs, $scaf1dir . $scaf1, $scaf2dir . $scaf2); 
			}
		    }
		    else { push(@scafpairs,$scaf1,$scaf2); }
		}
		my @ordori2=();
		push(@ordori2,$scafpairs[0]);
		for (my $scaffA=1; $scaffA<$#scafpairs; $scaffA+=2) {
		    my $scaffB=$scaffA+1;
		    if($scaffB==$#scafpairs) {next;}
		    if($scafpairs[$scaffA] eq $scafpairs[$scaffB]) { push(@ordori2,$scafpairs[$scaffA]); }
		    else { 
			print LOG "Unmatched colinearity: " . join(" ",@scafpairs) . "\n";
			print LOG "CONFLICT: $scafpairs[$scaffA] !! $scafpairs[$scaffB]\n";
			$conflicts{$scafpairs[$scaffB]}=$scaffB/2;
		    }
		}
		push(@ordori2,$scafpairs[$#scafpairs]);
		undef my %subsets;
		if(scalar(keys %conflicts)>0) {
		    my $subset=0;
		    undef my %conflict_original;
		    foreach my $conf ( sort { $conflicts{$a} <=> $conflicts{$b} } keys %conflicts) { $conflict_original{$scaffs_orig[$conflicts{$conf}]}=$conflicts{$conf}; }
		    for (my $scnt=0; $scnt<=$#scaffs_orig; $scnt++) {
			if(defined($conflict_original{$scaffs_orig[$scnt]})) { $subset++; }
			else { push(@{$subsets{$subset}},$scaffs[$scnt]); }
		    }
		}

		if(scalar(keys %subsets)>0) {
		    foreach my $ss (sort { $subsets{$a} <=> $subsets{$b} } keys %subsets) {
			if(scalar(@{$subsets{$ss}})>1) {
			    print LOG "SALVAGED: $ss\t" . join(" ",@{$subsets{$ss}}) . "\n";
			    my @unsorted=@{$subsets{$ss}};
			    my $fist=$unsorted[0];
			    my $last=$unsorted[$#unsorted];
			    $fist=~s/^.//;
			    $last=~s/^.//;
			    my($f,$l)=sort($fist,$last);
			    if($f eq $fist) { print OUT join(" ",@{$subsets{$ss}}) . "\n"; }
			    else {
				my @sorted=();
				foreach my $usr (reverse(@unsorted)) {
				    my $dir=substr($usr,0,1);
				    $usr=~s/^.//;
				    if($dir eq '+') { $dir='-'; }
				    elsif($dir eq '-') { $dir='+'; }
				    push(@sorted, $dir . $usr);
				}
				print OUT join(" ",@sorted) . "\n";
			    }
			}
		    }
		    print LOG "\n";
		}
		else { print OUT join(" ",@ordori2) . "\n"; }
		foreach my $conf (keys %conflicts, keys %conflicts2) {
		    if(defined($all_conflicts{"$spec\t$conf"})) { next; }
		    if($conf=~/^[\+\-\?]/) { $conf=~s/^.//; }
		    $all_conflicts{"$spec\t$conf"}=1;
		}
	    }
	    else { print OUT "$line\n"; }
	}
    }
    close(OUT);
    print LOG "\n\n" . join("\n",sort(keys %all_conflicts)) . "\n";
    close(LOG);

    undef my %old2new;
    open(IN,"$run\_OrthoStitch.res4.adjacencies_all.txt") || die $!;
    my @lines=<IN>;
    close(IN);
    open(OUT,">$run\_OrthoStitch.res4.1.adjacencies_all-ori.txt") || die $!;
    print OUT "$lines[0]";
    shift(@lines);
    foreach my $line (@lines) {
	chomp($line);
	my @adjs=split(/\t/,$line);
	if(defined($all_conflicts{"$adjs[0]\t$adjs[1]"})) { next; }
	if(defined($all_conflicts{"$adjs[0]\t$adjs[2]"})) { next; }
	if($adjs[3] eq '?' || $adjs[4] eq '?') {
	    if(defined($orientated{"$adjs[0]\t$adjs[1]\t$adjs[2]"})) {
		$adjs[$#adjs]=$adjs[$#adjs].";$adjs[3]\/$adjs[4]";
		($adjs[3],$adjs[4])=split(/\t/,$orientated{"$adjs[0]\t$adjs[1]\t$adjs[2]"});
		print OUT join("\t",@adjs) . "\n";
		$old2new{$line}=join("\t",@adjs);
	    }
	    elsif(defined($orientated{"$adjs[0]\t$adjs[2]\t$adjs[1]"})) {
		print "ONLY REV defined\n";
		#print OUT "$line\t" . $orientated{"$adjs[0]\t$adjs[2]\t$adjs[1]"} . "\tREV\n";
	    }
	    else { print OUT "$line\n"; $old2new{$line}=$line; }
	}
	else { print OUT "$line\n"; $old2new{$line}=$line; }
	push(@{$spec2finalpairs{$adjs[0]}},$old2new{$line});
    }
    close(OUT);

    open(IN,"$run\_OrthoStitch.res5.adjacencies_confident.txt") || die $!;
    my @lines=<IN>;
    close(IN);
    open(OUT,">$run\_OrthoStitch.res5.1.adjacencies_confident-ori.txt") || die $!;
    print OUT "$lines[0]";
    shift(@lines);
    foreach my $line (@lines) {
	chomp($line);
	if(defined($old2new{$line})) { 
	    print OUT "$old2new{$line}\n";
	    my @adjs=split(/\t/,$line);
	    push(@{$spec2finalconfi{$adjs[0]}},$old2new{$line});
	}
    }
    close(OUT);

    open(IN,"$run\_OrthoStitch.res6.adjacencies_superconfident.txt") || die $!;
    my @lines=<IN>;
    close(IN);
    open(OUT,">$run\_OrthoStitch.res6.1.adjacencies_superconfident-ori.txt") || die $!;
    print OUT "$lines[0]";
    shift(@lines);
    foreach my $line (@lines) {
	chomp($line);
	if(defined($old2new{$line})) { 
	    print OUT "$old2new{$line}\n"; 
	    my @adjs=split(/\t/,$line);
	    push(@{$spec2finalsucon{$adjs[0]}},$old2new{$line});
	}
    }
    close(OUT);

}
close(LOG1);
print "OK.\n";


print INP "\n# 6. Final adjacencies\n";
print INP "Species ID, all, confident, superconfident\n";
foreach my $sp (sort keys %ancsp) {
    my $pairs=0;
    my $confi=0;
    my $super=0;
    if(defined(${$spec2finalpairs{$sp}}[0])) { $pairs=scalar(@{$spec2finalpairs{$sp}}); }
    if(defined(${$spec2finalconfi{$sp}}[0])) { $confi=scalar(@{$spec2finalconfi{$sp}}); }
    if(defined(${$spec2finalsucon{$sp}}[0])) { $super=scalar(@{$spec2finalsucon{$sp}}); }
    print INP "\t$sp\t$pairs\t$confi\t$super\n";
}
print INP "\n\n";
print INP "COMPLETED: " . `date` . "\n";
close(INP);
print "\nDONE: OrthoStritch.pl " . join(" ",@ARGV) . "\n\n";

