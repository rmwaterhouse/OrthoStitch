# OrthoStitch

Scaffold adjacencies from conserved orthologous neighbours  


Using gene orthology data from cross-species comparisons, OrthoStitch identifies genes located at scaffold extremities and evaluates the evidence from the locations of orthologous genes from other species to predict likely scaffold adjacencies. The analysis proceeds in a stepwise manner, first identifying the most likely neighbour for each scaffold end and then requiring best neighbours to be reciprocal in order to identify putative adjacencies. The evaluations are not limited to single-copy orthologues as analyses of all paralogues are performed such that all possible neighbour relationships are examined. Putative neighbours at scaffold extremities are scored by how many of the species with orthologues show the same neighbour relationship, requiring at least two species to do so.  

OrthoStitch requires as input an anchor groups file and an anchor locations file.  
- The anchor groups file may be generated from any orthology delineation procedure, and consists of just three columns of data: the orthologous group identifier, the gene identifier, and the species identifier.  
- The anchor locations file may be generated from general feature format (GFF) or general transfer format (GTF) files that indicate the genomic locations of annotated features (genes) for each assembly.  
- Like GFF or GTF files, the anchor locations file consists of nine columns, with only the coding sequence (CDS) lines selected from GFF or GTF files, and with the ‘source’ column (2nd column) containing the species identifier, and with the ‘attribute’ column (9th column) containing only the gene identifier.  
- The gene and species identifiers used in both the groups file and the locations file must match exactly, and gene identifiers must be unique across the complete dataset of all species.  
- The anchor locations file may contain the locations of genes that are not present in the anchor groups file, i.e. some genes with known locations may not have been assigned to any orthologous group, however, the anchor groups file may not contain any genes that are not present in the anchor locations files, i.e. all genes in orthologous groups must have known locations.  

OrthoStitch options allow for the genomic location of each anchor gene to be set as the start, middle, or end of the input coding sequence genomic coordinates, and the analyses can be run using only genes with orthologues or with all genes in the locations file.  

All predicted adjacencies are further classified into confident, and superconfident subsets.  
- Confident adjacencies require more than a third of comparison species to have orthologues and more than a third of those that do have orthologues to support the predicted scaffold adjacency.  
- Superconfident adjacencies additionally require the same of their upstream or downstream neighbours.  

The adjacency score for each pair of putatively neighbouring scaffolds is computed as the product of a synteny score (S) and a universality score (U), based on the numbers of species with orthologues that support the adjacency where Sup = the number of supporting species, Pos = the number of possible species, and Tot = the total number of species thus:  
- S = (Sup1/Pos1) + (Sup2/Pos2) / 2  
- U = ( (Pos1+Pos2)/2 ) / (Tot-1)





Example anchor groups file format with tab-separated [1] orthologous group identifier [2] gene identifier [3] species identifier  
EOG09170006     AMIN010487      AMINI  
EOG09170006     ACOM025653      ACOLU  
EOG09170006     AEPI009236      AEPIR  
EOG09170006     AMEM011363      AMERU  
EOG09170006     AFUN002071      AFUNE  
EOG09170006     AFAF000211      AFARA  
EOG09170006     AGAP007388      AGAMB  
EOG09170006     AALB005751      AALBI  
EOG09170006     AARA008002      AARAB  
EOG09170006     AMAM017764      AMACU  
EOG09170006     AMAM021646      AMACU  
EOG09170006     ASIC019697      ASINC  
EOG09170006     ASIS018864      ASINS  
EOG09170007     AMIN003326      AMINI  
EOG09170007     ACOM037865      ACOLU  
EOG09170007     AEPI009586      AEPIR  
EOG09170007     AMEM016177      AMERU  
EOG09170007     AFUN001717      AFUNE  
EOG09170007     AFAF020208      AFARA  
EOG09170007     AGAP002015      AGAMB  
EOG09170007     AALB007133      AALBI  
EOG09170007     AARA006929      AARAB  
EOG09170007     AMAM000048      AMACU  
EOG09170007     ASIC004662      ASINC  
EOG09170007     ASIS002489      ASINS  




Example anchor locations file format with tab-separated [1] scaffold/chromosome identifier [2] species identifier [3] CDS [4] start [5] end [6] score [7] strand [8] phase and [9] gene identifier  
KB663610        AMINI   CDS     10168266        10168533        .       -       1       AMIN002969  
KB663610        AMINI   CDS     10175754        10175761        .       -       0       AMIN002969  
KB663610        AMINI   CDS     10176563        10176666        .       -       2       AMIN002970  
KB663610        AMINI   CDS     10176751        10176879        .       -       2       AMIN002970  
KB663610        AMINI   CDS     10176985        10177240        .       -       0       AMIN002970  
KB663610        AMINI   CDS     10177561        10177760        .       -       2       AMIN002970  
KB663610        AMINI   CDS     10177830        10179798        .       -       0       AMIN002970  
KB663610        AMINI   CDS     10202151        10202240        .       -       0       AMIN002971  
KB663610        AMINI   CDS     10202306        10203331        .       -       0       AMIN002971  
KB663610        AMINI   CDS     10203398        10203574        .       -       0       AMIN002971  
KB663610        AMINI   CDS     10203922        10203970        .       -       1       AMIN002971  





OUTPUT LOGS:  

xxx_OrthoStitch.log1.genes.txt  
Log of gene neighbour evidence gathering.  

xxx_OrthoStitch.log2.evidence.txt  
Summary gene pair neighbour evidence.  

xxx_OrthoStitch.log3.reciprocalpairs.txt  
Gene pair neighbours that pass reciprocity test.  

xxx_OrthoStitch.log4.reciprocalcsets.txt  
Gene set neighbours that pass reciprocity test.  

xxx_OrthoStitch.log5.multiadjacencies.txt  
Log of tests for compatible multi-neighbours.  

xxx_OrthoStitch.log6.orientations.txt  
Log of orientation enhancement steps.  

xxx_OrthoStitch.log7.summaries.txt  
Summary log of OrthoStitch analysis run.  


OUTPUT RESULTS:  

xxx_OrthoStitch.res1.simple_adjacencies_all.txt  
List of initial adjacencies.  

xxx_OrthoStitch.res2.complex_adjacencies_resolved.txt  
List of resolved complex adjacencies.  

xxx_OrthoStitch.res3.multi-adjacencies.txt  
List of pair- and multi-adjacencies.  

xxx_OrthoStitch.res3.1.multi-adjacencies-conflicts.txt  
List of attempts to resolved conflicting adjacencies.  

xxx_OrthoStitch.res3.1.multi-adjacencies-ori.txt  
List of attempts to orient adjacencies.  

xxx_OrthoStitch.res4.adjacencies_all.txt  
List of all identified adjacencies.  

xxx_OrthoStitch.res4.1.adjacencies_all-ori.txt  
List of all identified adjacencies after orientation enhancement.  

xxx_OrthoStitch.res5.adjacencies_confident.txt  
List of confident identified adjacencies.  

xxx_OrthoStitch.res5.1.adjacencies_confident-ori.txt  
List of confident identified adjacencies after orientation enhancement.  

xxx_OrthoStitch.res6.adjacencies_superconfident.txt  
List of superconfident identified adjacencies.  

xxx_OrthoStitch.res6.1.adjacencies_superconfident-ori.txt  
List of superconfident identified adjacencies after orientation enhancement.  

xxx_OrthoStitch.res7.adjacencies_evidencemapped.txt  
Full evidence for all adjacencies.  

